#include <bits/stdc++.h>

using namespace std;

bool isPalindrome (int n) {
	string s = to_string (n);
	string t = s;
	reverse (t.begin(), t.end());
	return (s == t);
}

int largestPalindromeProduct (int numberOfDigits) {
	int start = pow (10, (numberOfDigits - 1));
	int end = pow (10, numberOfDigits) - 1;
	int maxi = 0;
	for (int i = end; i >= start; i --)
		for (int j = end; j >= i; j --)
			if (isPalindrome (i * j))
				maxi = max (maxi, i * j);
	return maxi;
}

int32_t main(){
    cout << largestPalindromeProduct (3);
    return 0;
}
