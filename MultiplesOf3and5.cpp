#include <bits/stdc++.h>

using namespace std;

int multiples_3_5 (int limit) {
	int sum = 0;
	for (int i = 1; i < limit; i ++)
		if (i % 3 == 0 || i % 5 == 0)
			sum += i;
	return sum;
}

int32_t main()
{
    cout << multiples_3_5 (1000);
    return 0;
}
