#include <bits/stdc++.h>

using namespace std;

#define int long long

bool isPrime (int n) {
	if (n == 1)
		return false;
	for (int i = 2; i * i <= n; i++)
		if (n % i == 0)
			return false;
	return true;
}

int largestPrimeFactor (int number) {
	for (int i = sqrt(number) + 1; i >= 2; i --)
		if (number % i == 0 && isPrime (i))
			return i;
	return number;
}

int32_t main()
{
    cout << largestPrimeFactor (600851475143);
    return 0;
}
