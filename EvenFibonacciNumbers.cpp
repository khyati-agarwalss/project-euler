#include <bits/stdc++.h>

using namespace std;

int sumOfEvenFibonacci (int limit) {
	int a = 0, b = 1, c = 1;
	int sum = 0;
	while (c <= limit) {
		c = a + b;
		a = b;
		b = c;
		if (c % 2 == 0)
			sum += c;
	}
	return sum;
}

int32_t main(){
    cout << sumOfEvenFibonacci (4000000);
    return 0;
}
